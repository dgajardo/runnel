#include "aaigriddata.h"
#include <QFile>
#include <QTextStream>
#include <terrain.h>

AAIGridData::AAIGridData()
{

}

AAIGridData::~AAIGridData()
{

}

bool AAIGridData::loadData(QString name, Terrain *terrain)
{
    /* TODO: Error handling if file contents are not in the expected format */

    QFile file(name);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream stream(&file);
    QString line;

    double xllCorner;
    double yllCorner;
    double yulCorner;
    double dx;
    double dy;
    double nodata;

    line = stream.readLine().simplified();
    terrain->width = line.split(" ").at(1).toInt();
    line = stream.readLine().simplified();
    terrain->height = line.split(" ").at(1).toInt();

    line = stream.readLine().simplified();
    xllCorner = line.split(" ").at(1).toDouble();
    line = stream.readLine().simplified();
    yllCorner = line.split(" ").at(1).toDouble();

    line = stream.readLine().simplified();
    dx = line.split(" ").at(1).toDouble();
    line = stream.readLine().simplified();
    dy = line.split(" ").at(1).toDouble();

    yulCorner = yllCorner + (terrain->height - 1.0)*dy;

    line = stream.readLine().simplified();
    nodata = line.split(" ").at(1).toDouble();

    int nodeCounter = 0;
    for (int i = 0; i < terrain->height; i++) {
        double nodePositionY = yulCorner - i*dy;
        line = stream.readLine().trimmed();
        QStringList elevationData = line.split(" ");
        for (int j = 0; j < terrain->width && j < elevationData.length(); j++) {
            double nodePositionX = xllCorner + j*dx;
            double nodeElevation = elevationData.at(j).toDouble();
            glm::vec3 coords = glm::vec3(nodePositionX, nodePositionY, nodeElevation);
            runnel::Point *node = new runnel::Point(coords, nodeCounter);
            terrain->setBoundingBox(coords);
            terrain->addPoint(node);
            nodeCounter++;
        }
    }

    return true;
}
