#ifndef AAIGRIDDATA_H
#define AAIGRIDDATA_H

#include <QFile>

class Terrain;
class QString;

class AAIGridData
{
public:
    AAIGridData();
    ~AAIGridData();
    bool loadData(QString name, Terrain *terrain);
};

#endif // AAIGRIDDATA_H
