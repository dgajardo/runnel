#include "gmpitremover.h"
#include <algorithm>

GMPitRemover::GMPitRemover()
{
}

GMPitRemover::~GMPitRemover()
{

}

void GMPitRemover::run(Terrain *ter)
{
    this->ter = ter;
    std::vector<runnel::Point*> inflowSinks;

    for (runnel::Point *node : ter->struct_point) {
        node->flags = 0;
        if (isInflowSink(node)) {
            inflowSinks.push_back(node);
        }
    }

    for (runnel::Point *inflowSink : inflowSinks) {
        if (inflowSink->flags == 1) {
            continue;
        }
        int windowSize = BASE_WINDOW_SIZE;
        bool potentialOutletFound = false;

        std::vector<runnel::Point*> contributingArea;
        std::vector<runnel::Point*> potentialOutlets;
        runnel::Point* lowestOutlet;

        std::vector<runnel::Point*> potentialBreachingSites;
        runnel::Point* breachingSite;

        while (!potentialOutletFound) {
            if (windowSize > ter->width || windowSize > ter->height ||
                    windowSize > 30) {
                break;
            }
            contributingArea = computeContributingArea(inflowSink, windowSize,
                                                       contributingArea);
            potentialOutlets = findPotentialOutlets(contributingArea);

            if (potentialOutlets.size() == 0) {
                windowSize += 2;
                continue;
            }

            lowestOutlet = getLowestPotentialOutlet(potentialOutlets,
                                                    contributingArea);

            std::vector<int> windowBoundaries = defineWindowBoundaries(inflowSink,
                                                                       windowSize);
            std::vector<int> boundaryContributorIds = getBoundaryContributors(windowBoundaries,
                                                                              contributingArea);

            if (!hasBoundaryContributorBelowLowestOutlet(boundaryContributorIds,
                                                         lowestOutlet)) {
                potentialOutletFound = true;
            } else {
                windowSize += 2;
            }
        }

        if (potentialOutletFound == false) {
            continue;
        }

        if (!containsClosedDepressions(contributingArea, lowestOutlet)) {
            for (runnel::Point *node : contributingArea) {
                if (node->coord.z == lowestOutlet->coord.z) {
                    flagNode(node);
                }
            }
        } else {
            potentialBreachingSites = findPotentialBreachingSites(contributingArea,
                                                                  lowestOutlet);

            if (potentialBreachingSites.size() == 0) {
                fillDepressions(contributingArea, lowestOutlet);
            } else if (potentialBreachingSites.size() > 1) {
                breachingSite = getSteepestSlopeBreachingSite(potentialBreachingSites,
                                                              contributingArea);
            } else {
                breachingSite = potentialBreachingSites.front();
            }

            performBreaching(breachingSite, contributingArea);
            fillDepressions(contributingArea, breachingSite);
        }
    }
}

std::vector<runnel::Point*> GMPitRemover::computeContributingArea(runnel::Point *centralInflowSink,
                                                                  int windowSize,
                                                                  std::vector<runnel::Point*> contributingArea)
{
    std::vector<runnel::Point*> pendingNodes;
    std::vector<int> windowBoundaries = defineWindowBoundaries(centralInflowSink, windowSize);

    if (!contributingArea.empty()) {
        std::vector<int> boundaryIds = getBoundaryIds(windowBoundaries);
        for (int nodeId : boundaryIds) {
            runnel::Point* node = ter->struct_point[nodeId];
            if (std::find(contributingArea.begin(), contributingArea.end(),
                          node) != contributingArea.end()) {
                continue;
            }
            for (runnel::Point* neighbor : computeNeighborhood(node, windowBoundaries)) {
                if (std::find(contributingArea.begin(), contributingArea.end(),
                                  neighbor) != contributingArea.end() &&
                        node->coord.z >= neighbor->coord.z) {
                    pendingNodes.push_back(node);
                    break;
                }
            }
        }
    } else {
        pendingNodes.push_back(centralInflowSink);
    }

    while (!pendingNodes.empty()) {
        std::vector<runnel::Point*> newNodes;

        for (runnel::Point* node : pendingNodes) {
            contributingArea.push_back(node);
            for (runnel::Point* neighbor : computeNeighborhood(node, windowBoundaries)) {
                if (neighbor->coord.z >= node->coord.z &&
                        std::find(contributingArea.begin(), contributingArea.end(),
                                  neighbor) == contributingArea.end() &&
                        std::find(pendingNodes.begin(), pendingNodes.end(),
                                  neighbor) == pendingNodes.end() &&
                        std::find(newNodes.begin(), newNodes.end(),
                                  neighbor) == newNodes.end()) {
                    newNodes.push_back(neighbor);
                }
            }
        }

        pendingNodes = newNodes;
    }

    return contributingArea;
}

std::vector<runnel::Point *> GMPitRemover::computeNeighborhood(runnel::Point *node)
{
    std::vector<int> windowBoundaries = {0, (int)ter->struct_point.size() - 1};
    return computeNeighborhood(node, windowBoundaries);
}

std::vector<runnel::Point*> GMPitRemover::computeNeighborhood(runnel::Point *node,
                                                               std::vector<int> windowBoundaries)
{
    std::vector<runnel::Point*> neighbors;

    int upperLeftId = windowBoundaries[0];
    int lowerRightId = windowBoundaries[1];

    int nodeId = node->ident;

    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            int neighborId = nodeId + j*ter->width + i;
            if (neighborIndexIsOutOfBounds(nodeId, neighborId) ||
                    neighborId < upperLeftId ||
                    neighborId > lowerRightId ||
                    neighborId % ter->width < upperLeftId % ter->width ||
                    neighborId % ter->width > lowerRightId % ter->width) {
                continue;
            } else {
                neighbors.push_back(ter->struct_point[neighborId]);
            }
        }
    }

    return neighbors;
}

bool GMPitRemover::containsClosedDepressions(std::vector<runnel::Point*> contributingArea,
                                             runnel::Point *lowestOutlet)
{
    for (runnel::Point* node : contributingArea) {
        if (node->coord.z < lowestOutlet->coord.z) {
            return true;
        }
    }

    return false;
}

std::vector<int> GMPitRemover::defineWindowBoundaries(runnel::Point *inflowSink, int size)
{
    std::vector<int> windowCornerIds;
    // The window must be centered on inflowSink. We assume 'size' is odd

    int nodeId = inflowSink->ident;

    // Booleans to indicate if the respective side must be shrunk to DEM boundaries
    bool constrainLeft = false;
    bool constrainUpper = false;
    bool constrainRight = false;
    bool constrainLower = false;

    if (nodeId % ter->width < size/2) {
        constrainLeft = true;
    }
    if (nodeId - ter->width*(size/2) < 0) {
        constrainUpper = true;
    }
    if (nodeId % ter->width >= ter->width - size/2) {
        constrainRight = true;
    }
    if (nodeId + ter->width*(size/2) >= (int)ter->struct_point.size()) {
        constrainLower = true;
    }

    int leftOffset = constrainLeft ?
                -1*(nodeId % ter->width) :
                -1*(size/2);
    int upperOffset = constrainUpper ?
                -1*(nodeId / ter->height) :
                -1*(size/2);
    int rightOffset = constrainRight ?
                ter->width - (nodeId % ter->width) - 1 :
                (size/2);
    int lowerOffset = constrainLower ?
                ter->height - (nodeId / ter->width) - 1 :
                (size/2);

    int upperLeftId = nodeId + leftOffset + upperOffset*ter->width;
    int lowerRightId = nodeId + rightOffset + lowerOffset*ter->width;

    windowCornerIds = {upperLeftId, lowerRightId};
    return windowCornerIds;
}

void GMPitRemover::fillDepressions(std::vector<runnel::Point*> contributingArea,
                                   runnel::Point *breachingSite)
{
    std::vector<runnel::Point*> lowerElevContributors;

    std::copy_if(contributingArea.begin(), contributingArea.end(),
                 std::back_inserter(lowerElevContributors),
                 [breachingSite] (runnel::Point* p) {
                     return p->coord.z < breachingSite->coord.z;
                 });

    for (runnel::Point* contributor : lowerElevContributors) {
        contributor->coord.z = breachingSite->coord.z;
        flagNode(contributor);
    }
}

std::vector<runnel::Point*> GMPitRemover::findPotentialBreachingSites(std::vector<runnel::Point*>
                                                                      contributingArea,
                                                                      runnel::Point*
                                                                      lowestOutlet)
{
    // Nodes in contributing area whose elevation == lowestOutlet's elevation
    std::vector<runnel::Point*> sameElevContributors;

    // Nodes in contributing area whose elevation < lowestOutlet's elevation
    std::vector<runnel::Point*> lowerElevContributors;

    // Nodes in sameElevContributors adjacent to a node: 1) outside the
    // contributing area && 2) with elevation < lowestOutlet's elevation
    std::vector<runnel::Point*> outflowingBoundaryNodes;

    std::vector<runnel::Point*> potentialBreachingSites;

    std::copy_if(contributingArea.begin(), contributingArea.end(),
                 std::back_inserter(sameElevContributors),
                 [lowestOutlet] (runnel::Point* p) {
                     return p->coord.z == lowestOutlet->coord.z;
                 });

    std::copy_if(contributingArea.begin(), contributingArea.end(),
                 std::back_inserter(lowerElevContributors),
                 [lowestOutlet] (runnel::Point* p) {
                     return p->coord.z < lowestOutlet->coord.z;
                 });

    for (runnel::Point* node : sameElevContributors) {
        for (runnel::Point* neighbor : computeNeighborhood(node)) {
            if (std::find(contributingArea.begin(),
                          contributingArea.end(),
                          neighbor) == contributingArea.end() &&
                    neighbor->coord.z < lowestOutlet->coord.z) {
                outflowingBoundaryNodes.push_back(node);
                break;
            }
        }
    }

    for (runnel::Point* node : outflowingBoundaryNodes) {
        for (runnel::Point* lowerContributor : lowerElevContributors) {
            if (nodeDistance(node, lowerContributor) <= MAX_BREACHING_LENGTH) {
                potentialBreachingSites.push_back(node);
                break;
            }
        }
    }

    return potentialBreachingSites;
}

std::vector<runnel::Point*> GMPitRemover::findPotentialOutlets(std::vector<runnel::Point *> contributingArea)
{
    std::vector<runnel::Point*> potentialOutlets;

    for (runnel::Point* node : contributingArea) {
        for (runnel::Point* neighbor : computeNeighborhood(node)) {
            if (std::find(contributingArea.begin(),
                          contributingArea.end(),
                          neighbor) == contributingArea.end() &&
                    node->coord.z > neighbor->coord.z) {
                potentialOutlets.push_back(node);
                break;
            }
        }
    }

    return potentialOutlets;
}

void GMPitRemover::flagLowestOutletElevationNodes(std::vector<runnel::Point*> contributingArea,
                                                  runnel::Point* lowestOutlet)
{
    auto iter = std::remove_if(contributingArea.begin(), contributingArea.end(),
                               [lowestOutlet] (runnel::Point* p) {
                                   return p->coord.z != lowestOutlet->coord.z;
                               });
    contributingArea.erase(iter, contributingArea.end());

    for (runnel::Point* node : contributingArea) {
        flagNode(node);
    }
}

void GMPitRemover::flagNode(runnel::Point *node)
{
    node->flags = 1;
}

std::vector<int> GMPitRemover::getBoundaryContributors(std::vector<int> windowBoundaries,
                                                                  std::vector<runnel::Point*> contributingArea)
{
    std::vector<int> boundaryIds = getBoundaryIds(windowBoundaries);

    std::vector<int> boundaryContributorIds;
    std::copy_if(boundaryIds.begin(), boundaryIds.end(),
                 std::back_inserter(boundaryContributorIds),
                 [contributingArea] (int boundaryNodeId) {
                     return std::find_if(contributingArea.begin(),
                                         contributingArea.end(),
                                         [boundaryNodeId] (runnel::Point* node) {
                                             return node->ident == boundaryNodeId;
                                         }) != contributingArea.end();
                 });

    return boundaryContributorIds;
}

std::vector<int> GMPitRemover::getBoundaryIds(std::vector<int> windowBoundaries)
{
    std::vector<int> windowBoundaryIds;

    // We need the real size of the window, clipped to DEM boundaries
    int upperLeftId = windowBoundaries[0];
    int lowerRightId = windowBoundaries[1];

    int windowWidth = lowerRightId%ter->width - upperLeftId%ter->width + 1;
    int upperRightId = upperLeftId + windowWidth - 1;
    int windowHeight = (lowerRightId - upperRightId)/ter->width + 1;

    for (int i = 0; i < windowWidth; ++i) {
        windowBoundaryIds.push_back(upperLeftId + i);
        windowBoundaryIds.push_back(lowerRightId - i);
    }

    for (int j = 1; j < windowHeight - 1; ++j) {
        windowBoundaryIds.push_back(upperLeftId + j*ter->width);
        windowBoundaryIds.push_back(upperRightId + j*ter->width);
    }

    return windowBoundaryIds;
}

runnel::Point *GMPitRemover::getLowestPotentialOutlet(std::vector<runnel::Point *> potentialOutlets,
                                                      std::vector<runnel::Point *> contributingArea)
{
    std::vector<runnel::Point*> lowestOutlets;
    runnel::Point* resultOutlet;

    auto minElevIter = std::min_element(potentialOutlets.begin(),
                                        potentialOutlets.end(),
                                        [] (runnel::Point* p1, runnel::Point* p2) {
                                            return p1->coord.z < p2->coord.z;
                                        });

    std::copy_if(minElevIter, potentialOutlets.end(),
                 std::back_inserter(lowestOutlets),
                 [minElevIter] (runnel::Point* p) {
                     return p->coord.z == (*minElevIter)->coord.z;
                 });

    if (lowestOutlets.size() > 1) {
        resultOutlet = getSteepestSlopeOutlet(lowestOutlets, contributingArea);
    } else {
        resultOutlet = (*minElevIter);
    }

    return resultOutlet;
}

runnel::Point *GMPitRemover::getSteepestSlopeBreachingSite(std::vector<runnel::Point*> breachingSites,
                                                           std::vector<runnel::Point*> contributingArea)
{
    // Primary criterion: steepest slope to a node outside the contrib. area (outbound)
    std::vector<double> maxOutboundSlopeBySite;
    // Secondary criterion: steepest slope to a node within the contrib. area (inbound)
    std::vector<double> maxInboundSlopeBySite;

    for (runnel::Point* site : breachingSites) {
        maxOutboundSlopeBySite.push_back(0.0);
        maxInboundSlopeBySite.push_back(0.0);
        for (runnel::Point* neighbor : computeNeighborhood(site)) {
            double slope = site->coord.z - neighbor->coord.z;
            if (std::find(contributingArea.begin(),
                          contributingArea.end(),
                          neighbor) == contributingArea.end()) {
                if (slope > maxOutboundSlopeBySite.back()) {
                    maxOutboundSlopeBySite.back() = slope;
                }
            } else {
                if (slope > maxInboundSlopeBySite.back()) {
                    maxInboundSlopeBySite.back() = slope;
                }
            }
        }
    }

    auto maxOutboundSlopeIter = std::max_element(maxOutboundSlopeBySite.begin(),
                                                 maxOutboundSlopeBySite.end());
    auto maxInboundSlopeIter = std::max_element(maxInboundSlopeBySite.begin(),
                                                maxInboundSlopeBySite.end());

    int maxOutboundIndex = maxOutboundSlopeIter - maxOutboundSlopeBySite.begin();
    int maxInboundIndex = maxInboundSlopeIter - maxInboundSlopeBySite.begin();

    int selectedIndex;

    if (std::count(maxOutboundSlopeBySite.begin(), maxOutboundSlopeBySite.end(),
                   *maxOutboundSlopeIter) == 1) {
        selectedIndex = maxOutboundIndex;
    } else if (std::count(maxInboundSlopeBySite.begin(), maxInboundSlopeBySite.end(),
                          *maxInboundSlopeIter) == 1) {
        selectedIndex = maxInboundIndex;
    } else {
        // If more than one site fulfills each criterion, we arbitrarily return
        // the first one we encountered (i.e. the smallest index)
        selectedIndex = glm::min(maxOutboundIndex, maxInboundIndex);
    }

    return breachingSites[selectedIndex];
}

runnel::Point *GMPitRemover::getSteepestSlopeOutlet(std::vector<runnel::Point *> potentialOutlets,
                                                    std::vector<runnel::Point *> contributingArea)
{
    std::vector<double> maxSlopeByOutlet;

    for (runnel::Point* outlet : potentialOutlets) {
        maxSlopeByOutlet.push_back(0.0);
        for (runnel::Point* neighbor : computeNeighborhood(outlet)) {
            if (std::find(contributingArea.begin(),
                          contributingArea.end(),
                          neighbor) == contributingArea.end()) {
                double slope = outlet->coord.z - neighbor->coord.z;
                if (slope > maxSlopeByOutlet.back()) {
                    maxSlopeByOutlet.back() = slope;
                }
            }
        }
    }

    auto maxSlopeIter = std::max_element(maxSlopeByOutlet.begin(), maxSlopeByOutlet.end());
    int steepestSlopeOutletIndex = maxSlopeIter - maxSlopeByOutlet.begin();
    return potentialOutlets[steepestSlopeOutletIndex];
}

bool GMPitRemover::hasBoundaryContributorBelowLowestOutlet(std::vector<int> contributorIds,
                                                           runnel::Point* lowestOutlet)
{
    for (int id : contributorIds) {
        if (ter->struct_point[id]->coord.z < lowestOutlet->coord.z) {
            return true;
        }
    }

    return false;
}

bool GMPitRemover::hasHigherNeighbor(runnel::Point *node)
{
    for (runnel::Point* neighbor : computeNeighborhood(node)) {
        if (neighbor->coord.z > node->coord.z) {
            return true;
        }
    }

    return false;
}

bool GMPitRemover::hasLowerNeighbor(runnel::Point *node)
{
    for (runnel::Point* neighbor : computeNeighborhood(node)) {
        if (neighbor->coord.z < node->coord.z) {
            return true;
        }
    }

    return false;
}

bool GMPitRemover::isBoundaryNode(runnel::Point *node)
{
    int nodeId = node->ident;

    return nodeId < ter->width ||
            nodeId >= (ter->width)*(ter->height - 1) ||
            nodeId % ter->width == 0 ||
            nodeId % ter->width == ter->width - 1;
}

bool GMPitRemover::isInflowSink(runnel::Point *node)
{
    return !isBoundaryNode(node) &&
            hasHigherNeighbor(node) &&
            !hasLowerNeighbor(node);
}

// Duplicate code (GMFlatResolver class)
// This code may also be useful to other algorithms.
// TODO refactor this code
bool GMPitRemover::neighborIndexIsOutOfBounds(int nodeId, int neighborId)
{
    if (neighborId < 0 || neighborId > (int)ter->struct_point.size()-1 ||
            nodeId == neighborId) {
        return true;
    }
    if ((nodeId%ter->width == 0 && neighborId%ter->width == ter->width - 1) ||
            (nodeId%ter->width == ter->width - 1 && neighborId%ter->width == 0)) {
        return true; // neighborId is not a true neighbor in the DEM grid!
    }
    return false;
}

int GMPitRemover::nodeDistance(runnel::Point *p1, runnel::Point *p2)
{
    // Compute the Chebyshev/chessboard distance between p1 and p2 on the grid.
    int x1 = p1->ident % ter->width;
    int x2 = p2->ident % ter->width;
    int y1 = p1->ident / ter->width;
    int y2 = p2->ident / ter->width;

    return glm::max(glm::abs(x1-x2), glm::abs(y1-y2));
}

void GMPitRemover::performBreaching(runnel::Point *breachingSite,
                                    std::vector<runnel::Point*> contributingArea)
{
    // We repeat the procedure used when finding potential breaching sites,
    // in order to determine the appropriate breaching length in this case.
    std::vector<runnel::Point*> lowerElevContributors;

    std::copy_if(contributingArea.begin(), contributingArea.end(),
                 std::back_inserter(lowerElevContributors),
                 [breachingSite] (runnel::Point* p) {
                     return p->coord.z < breachingSite->coord.z;
                 });

    std::vector<int> breachingLengths;

    for (runnel::Point* lowerContributor : lowerElevContributors) {
        breachingLengths.push_back(nodeDistance(breachingSite, lowerContributor));
    }

    int breachingLength = breachingLengths.empty() ?
                0 :
                *std::min_element(breachingLengths.begin(),
                                           breachingLengths.end());

    // We identify the neighbor to which flow should be directed from
    // the breaching site, and we lower the latter's elevation to match it

    for (runnel::Point* neighbor : computeNeighborhood(breachingSite)) {
        if (std::find(contributingArea.begin(),
                      contributingArea.end(),
                      neighbor) == contributingArea.end() &&
                neighbor->coord.z < breachingSite->coord.z) {
            breachingSite->coord.z = neighbor->coord.z;
            break;
        }
    }

    if (breachingLength > 1) {
        // We find the next node to be breached
        // TODO: adapt for higher max breaching lengths? only supporting MBL 2
        std::vector<int> contributingAreaDistance;

        for (runnel::Point* contributor : contributingArea) {
            contributingAreaDistance.push_back(nodeDistance(breachingSite, contributor));
        }

        // If there is more than one node with min. Chebyshev distance, we
        // arbitrarily pick the first one we encounter
        auto minDistanceIter = std::min_element(contributingAreaDistance.begin(),
                                                contributingAreaDistance.end());
        int minDistanceIndex = minDistanceIter - contributingAreaDistance.begin();
        contributingArea[minDistanceIndex]->coord.z = breachingSite->coord.z;
    }
}
