#ifndef GMPITREMOVER_H
#define GMPITREMOVER_H

#include "terrain.h"
#include <set>
#include <unordered_map>

class GMPitRemover
{
public:
    GMPitRemover();
    ~GMPitRemover();
    void run(Terrain* ter);

private:
    Terrain* ter;
    const int BASE_WINDOW_SIZE = 5;
    const int MAX_BREACHING_LENGTH = 2;
    std::vector<runnel::Point*> computeContributingArea(runnel::Point* centralInflowSink,
                                                        int windowSize,
                                                        std::vector<runnel::Point*> contributingArea);
    std::vector<runnel::Point*> computeNeighborhood(runnel::Point* node);
    std::vector<runnel::Point*> computeNeighborhood(runnel::Point* node,
                                                    std::vector<int> windowBoundaries);
    bool containsClosedDepressions(std::vector<runnel::Point*> contributingArea,
                                   runnel::Point* lowestOutlet);
    std::vector<int> defineWindowBoundaries(runnel::Point* inflowSink, int size);
    void fillDepressions(std::vector<runnel::Point*> contributingArea,
                         runnel::Point* breachingSite);
    std::vector<runnel::Point*> findPotentialBreachingSites(std::vector<runnel::Point*>
                                                            contributingArea,
                                                            runnel::Point*
                                                            lowestOutlet);
    std::vector<runnel::Point*> findPotentialOutlets(std::vector<runnel::Point*>
                                                     contributingArea);
    void flagLowestOutletElevationNodes(std::vector<runnel::Point*> contributingArea,
                                        runnel::Point* lowestOutlet);
    void flagNode(runnel::Point* node);
    std::vector<int> getBoundaryContributors(std::vector<int> windowBoundaries,
                                                        std::vector<runnel::Point*> contributingArea);
    std::vector<int> getBoundaryIds(std::vector<int> windowBoundaries);
    runnel::Point* getLowestPotentialOutlet(std::vector<runnel::Point*> potentialOutlets,
                                            std::vector<runnel::Point*> contributingArea);
    runnel::Point* getSteepestSlopeBreachingSite(std::vector<runnel::Point*> breachingSites,
                                                 std::vector<runnel::Point*> contributingArea);
    runnel::Point* getSteepestSlopeOutlet(std::vector<runnel::Point*> potentialOutlets,
                                          std::vector<runnel::Point*> contributingArea);
    bool hasBoundaryContributorBelowLowestOutlet(std::vector<int> contributorIds,
                                                 runnel::Point* lowestOutlet);
    bool hasHigherNeighbor(runnel::Point* node);
    bool hasLowerNeighbor(runnel::Point* node);
    bool isBoundaryNode(runnel::Point* node);
    bool isInflowSink(runnel::Point* node);
    bool neighborIndexIsOutOfBounds(int nodeId, int neighborId);
    int nodeDistance(runnel::Point* p1, runnel::Point* p2);
    void performBreaching(runnel::Point* breachingSite,
                          std::vector<runnel::Point*> contributingArea);
};

#endif // GMPITREMOVER_H
