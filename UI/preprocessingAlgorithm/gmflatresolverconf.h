#ifndef GARBRECHTMARTZCONF_H
#define GARBRECHTMARTZCONF_H

#include <QWidget>

namespace Ui {
class GMFlatResolverConf;
}

class GMFlatResolverConf : public QWidget
{
    Q_OBJECT

public:
    explicit GMFlatResolverConf(QWidget *parent = 0);
    ~GMFlatResolverConf();

private:
    Ui::GMFlatResolverConf *ui;

signals:
    void runFlatResolution();
    void changeIncrementation(double value);
};

#endif // GARBRECHTMARTZCONF_H
