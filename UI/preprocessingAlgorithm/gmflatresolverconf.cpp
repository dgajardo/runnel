#include "gmflatresolverconf.h"
#include "ui_gmflatresolverconf.h"

GMFlatResolverConf::GMFlatResolverConf(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GMFlatResolverConf)
{
    ui->setupUi(this);

    QObject::connect(ui->gmflatresolver_button, SIGNAL(clicked()), this, SIGNAL(runFlatResolution()));
    QObject::connect(ui->gmflatresolver_value, SIGNAL(valueChanged(double)), this, SIGNAL(changeIncrementation(double)));
}

GMFlatResolverConf::~GMFlatResolverConf()
{
    delete ui;
}
