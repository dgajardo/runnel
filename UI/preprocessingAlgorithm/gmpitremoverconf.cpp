#include "gmpitremoverconf.h"
#include "ui_gmpitremoverconf.h"

GMPitRemoverConf::GMPitRemoverConf(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GMPitRemoverConf)
{
    ui->setupUi(this);

    QObject::connect(ui->gmpitremover_button, SIGNAL(clicked()), this, SIGNAL(runPitRemoval()));
}

GMPitRemoverConf::~GMPitRemoverConf()
{
    delete ui;
}
