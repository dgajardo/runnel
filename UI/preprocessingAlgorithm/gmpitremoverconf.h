#ifndef GMPITREMOVERCONF_H
#define GMPITREMOVERCONF_H

#include <QWidget>

namespace Ui {
class GMPitRemoverConf;
}

class GMPitRemoverConf : public QWidget
{
    Q_OBJECT

public:
    explicit GMPitRemoverConf(QWidget *parent = 0);
    ~GMPitRemoverConf();

private:
    Ui::GMPitRemoverConf *ui;

signals:
    void runPitRemoval();
};

#endif // GMPITREMOVERCONF_H
